# asdf-purescript

## Install

```sh
asdf plugin-add nodejs https://github.com/toastal/asdf-purescript.git
```

Unlike [obmarg’s asdf-purescript](https://github.com/obmarg/asdf-purescript), there’s no Node/npm requirement.
